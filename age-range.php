<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Matches</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
h1 {
	text-align: center;
	text-decoration: underline;
}
</style>
</head>
<body>
	<h1> All Users </h1>
	<?php
require 'db_set.php';
$min = 18;
$max = 9999;
if (isset($_POST['min_age'])) {
$min = (int) $_POST['min_age'];
}
if (isset($_POST['max_age'])) {
$max = (int) $_POST['max_age'];
}
$stmt = $mysqli->prepare("select _name, email, pictureUrl, description, age from users order by age where age < ? and age > ?");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->bind_param('ii', $min, $max);

$stmt->execute();
 
$stmt->bind_result($o_name, $o_email, $o_URL, $o_desc, $o_age);
 
echo "<ul>\n";
echo "<table> <tr> <th> Name </th> <th> Email </th> <th> Age </th> <th> Description </th> <th> picture </th> </tr>";
while($stmt->fetch()){
	printf("\t<tr> <td> %s </td><td> %s </td><td> %s </td><td> %s </td><td> <img src='%s' alt='%s picture' style='width:300px;height:300px;'> </td></tr>\n",
		htmlspecialchars($o_name),
		htmlspecialchars($o_email),
		htmlspecialchars($o_age),
		htmlspecialchars($o_desc),
		$o_URL,
		htmlspecialchars($o_name)
	);
}
echo "</ul>\n";
$stmt->close();
?>
</br>
<form action="age-range.php" method="GET">
			<p>
				<label> Min age: <input type="number" name="min_age" placeholder="18" min="18" increment="1"/> </label> <br/>
				<label> Max age: <input type="number" name="max_age" placeholder="99" min="18" increment="1"/> </label> <br/>
			</p>
			<p>	
				<input type="submit" name="submit" value="Find Hot Singles" />
			</p>
		</form>
</div>
</body>
</html>