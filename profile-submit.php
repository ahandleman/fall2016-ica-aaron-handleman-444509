<?php
include 'db_set.php';
$set_arr = array("name", "email", "age", "desc", "submit");
if (!isset($_POST)) {
		printf("You need to get here with the submit profile page");
		exit;
}
	if(!isset($_POST['name'])) {
		printf("You need to have your name set");
		exit;
	}
	if(!isset($_POST['email'])) {
		printf("You need to have your email set");
		exit;
	}
	if(!isset($_POST['age'])) {
		printf("You need to have your age set");
		exit;
	}
	if($_POST['age'] < 18) {
		printf("GROW UP YOUNG SIR/SIRESS");
		exit;
	}
	if(!isset($_POST['desc'])) {
		printf("You need to have your description set");
		exit;
	}
	if(!isset($_POST['submit'])) {
		printf("You need to press submit");
		exit;
	}
	if(!isset($_FILES['uploaded_file'])) {
		printf("You need to upload a file");
		exit;
	}
$in_name = $_POST['name'];
$in_desc = $_POST['desc'];
$in_email = $_POST['email'];
$in_age = (int) $_POST['age'];

	#from wiki

$filename = basename($_FILES['uploaded_file']['name']);
	if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
		printf("Invalid filename");
		exit;
}
 
// Get the username and make sure it is valid
$full_path = sprintf("uploads/%s", $filename);
 
if( move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $full_path) ){
	//FROM WIKI
	$stmt = $mysqli->prepare("insert into users (_name, email, pictureUrl, description, age) values (?, ?, ?, ?, ?)");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
 	$in_URL = $full_path;
	$stmt->bind_param('ssssi', $in_name, $in_email, $in_URL, $in_desc, $in_age);
	 
	$stmt->execute();
 
	$stmt->close();
	header("Location: show-users.php");
	exit;
}else{
	printf("failed upload");
	exit;
}
?>