CREATE database matchmaker; 
USE matchmaker;
# Copied and edited from module 3 code
# Give the news_bot user that was made earlier with a secret password CRUD
# Permissions over the news database
CREATE USER 'match_bot'@'localhost' IDENTIFIED BY 'pass';
GRANT ALL PRIVILEGES ON *.* TO 'match_bot'@'localhost' REQUIRE NONE WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
# Table of users and passwords
CREATE TABLE users(
	id MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
	_name VARCHAR(60) NOT NULL,
	email VARCHAR(255) NOT NULL,
	pictureUrl VARCHAR(255),
	description TINYTEXT,
	age TINYINT UNSIGNED NOT NULL DEFAULT 99,
	posted TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (id),
	UNIQUE KEY uk_email (email_address)
) engine = INNODB DEFAULT character SET=utf8 COLLATE=utf8_general_ci;